# The annual project

An automatic babysitter installed on a child's computer and responsible for two main actions, reporting forbidden words and blocking sites. There are three components: the child's computer, the parents' computer and the server that connects them.